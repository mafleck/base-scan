import pyjapc
import time
import pickle
import csv    

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from functools import partial
from scipy import constants as ctc
from scipy import optimize
import os
import functionsBPM as fs
import itertools
from sys import argv
import subprocess

########################################
# Set dry run/False real run/True
setRun = True
########################################

cycleUSER = 'LNA.USER.PBPRD1'
#cycleUSER = 'LNA.USER.MD4'
japc = pyjapc.PyJapc(selector='', incaAcceleratorName='ELENA', noSet=(not setRun))

try:
    japc.rbacLogin()
except:
    print('**** login failed, no set possible ****')
    pass

# status display for scan
plt.ion()
fig_scan, ax_scan = plt.subplots(4, 1, figsize=(9, 8), sharex=True)
fig_scan.canvas.set_window_title('Scan')
ax_scan[0].set_ylabel('X (mm)')
ax_scan[1].set_ylabel('Phi (mrad)')
ax_scan[2].set_ylabel('Y (mm)')
ax_scan[3].set_ylabel('Theta (mrad)')

## calulate regular grid points
#hOffsetScan = np.linspace(-5, 5, 3)
#hAngleScan  = np.linspace(-5, 5, 3)
#vOffsetScan = np.linspace(-5, 5, 3)
#vAngleScan  = np.linspace(-5, 5, 3)
#scan_iterable = list(itertools.product(hOffsetScan,hAngleScan,vOffsetScan,vAngleScan))

## alternatively read a file with grid points
scan_iterable = []
with open(argv[1]) as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        scan_iterable.append((float(row[0]),float(row[1]),float(row[2]),float(row[3])))

set_address = ['LNE07BEAM/BASE-H-OFFSET-mm',
               'LNE07BEAM/BASE-H-ANGLE-mrad',
               'LNE07BEAM/BASE-V-OFFSET-mm',
               'LNE07BEAM/BASE-V-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-H-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-V-ANGLE-mrad']

get_address = ['LNE07.BSGW.0722/FitAcq#gaussAmplitude','LNE07.BSGW.0722/FitAcq#gaussSigma','LNE07.BSGW.0722/FitAcq#gaussMean',
               'LNE07.BSGW.0722/FitAcq#intensity','LNE07.BSGW.0722/ReverseFit#gaussTg','LNE07.BSGW.0722/Acquisition#tangent',
               'LNE07.BSGW.0737/FitAcq#gaussAmplitude','LNE07.BSGW.0737/FitAcq#gaussSigma','LNE07.BSGW.0737/FitAcq#gaussMean',
               'LNE07.BSGW.0737/FitAcq#intensity','LNE07.BSGW.0737/ReverseFit#gaussTg','LNE07.BSGW.0737/Acquisition#tangent',
               'LNE07.BSGW.0744/FitAcq#gaussAmplitude','LNE07.BSGW.0744/FitAcq#gaussSigma','LNE07.BSGW.0744/FitAcq#gaussMean',
               'LNE07.BSGW.0744/FitAcq#intensity','LNE07.BSGW.0744/ReverseFit#gaussTg','LNE07.BSGW.0744/Acquisition#tangent']

exp = fs.Experiment(
    japc, 
    scan_iterable=scan_iterable, cycleUSER=cycleUSER,
    nrep=1, set_address=set_address, get_address=get_address,
    fig_scan=fig_scan, ax_scan=ax_scan)

japc.subscribeParam('LNE07.BSGW.0722/Acquisition#acqTime',
                    partial(fs.measurement, exp, timingSelectorOverride=cycleUSER), 
                    timingSelectorOverride=cycleUSER)
japc.startSubscriptions()
print('subscribed and running')

