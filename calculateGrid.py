import numpy as np
import csv

hOffsetStart = 0.72
hOffsetStop  = 0.72
hOffsetStep  = 1

hAngleStart = -5.5
hAngleStop  = -5.5
hAngleStep  = 2.0

vOffsetStart = -5.3
vOffsetStop  = 8.7
vOffsetStep  = 1.0

vAngleStart = -3.5
vAngleStop  = -3.5
vAngleStep  = 2.0

gridList = []

for vo in np.arange(vOffsetStart,vOffsetStop+vOffsetStep,step=vOffsetStep):
    for va in np.arange(vAngleStart,vAngleStop+vAngleStep,step=vAngleStep):
        for ha in np.arange(hAngleStart,hAngleStop+hAngleStep,step=hAngleStep):
            for ho in np.arange(hOffsetStart,hOffsetStop+hOffsetStep,step=hOffsetStep):
                #if (ha == -12) and (ho == 4.0):
                #    #print((-40.0,ha,vo,va))
                #    gridList.append((3,ha,vo,va))
                #elif (ha == -17.0) and (ho == -42.0):
                #    #print((-41.0,ha,vo,va))
                #    gridList.append((-41.0,ha,vo,va))
                #else:
                    #print((ho,ha,vo,va))
                #gridList.append((ho,ha,vo,va,0.0,0.0))
                gridList.append((ho,ha,vo,va,0.0,0.0))
        #gridList.append((2.0,-14.5,vo,va))
        #gridList.append((1.5,-17.0,vo,va))
        #gridList.append((1.0,-19.5,vo,va))
        #gridList.append((1.0,-22.0,vo,va))

print(gridList)

with open("./20211102_voffsetscan.csv", 'w') as f:
    writer = csv.writer(f,delimiter=',')
    for i,tup in enumerate(gridList):
        writer.writerow([tup[0],tup[1],tup[2],tup[3],tup[4],tup[5]])
        if i == 0:
            writer.writerow([tup[0],tup[1],tup[2],tup[3],tup[4],tup[5]])

