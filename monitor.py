import pyjapc
import time
import pickle
import csv    

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from functools import partial
from scipy import constants as ctc
from scipy import optimize
import os
import functions as fs
import itertools
from sys import argv
import subprocess

########################################
# Set dry run/False real run/True
setRun = True
########################################

cycleUSER = 'LNA.USER.PBPRD1'
#cycleUSER = 'LNA.USER.MD4'
japc = pyjapc.PyJapc(selector='', incaAcceleratorName='ELENA', noSet=(not setRun))

try:
    japc.rbacLogin()
except:
    print('**** login failed, no set possible ****')
    pass

set_address = ['LNE07BEAM/BASE-H-OFFSET-mm',
               'LNE07BEAM/BASE-H-ANGLE-mrad',
               'LNE07BEAM/BASE-V-OFFSET-mm',
               'LNE07BEAM/BASE-V-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-H-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-V-ANGLE-mrad']

get_address = ['LNE07.BSGW.0722/FitAcq#gaussAmplitude','LNE07.BSGW.0722/FitAcq#gaussSigma','LNE07.BSGW.0722/FitAcq#gaussMean',
               'LNE07.BSGW.0737/FitAcq#gaussAmplitude','LNE07.BSGW.0737/FitAcq#gaussSigma','LNE07.BSGW.0737/FitAcq#gaussMean',
               'LNE07.BSGW.0744/FitAcq#gaussAmplitude','LNE07.BSGW.0744/FitAcq#gaussSigma','LNE07.BSGW.0744/FitAcq#gaussMean',
               'LNE.VGPB.0749/PR#value']

exp = fs.Monitor(
    japc, 
    cycleUSER=cycleUSER,
    nrep=1, set_address=set_address, get_address=get_address)

japc.subscribeParam('LNE07.BSGW.0722/Acquisition#acqTime',
                    partial(fs.measurement, exp, timingSelectorOverride=cycleUSER), 
                    timingSelectorOverride=cycleUSER)
japc.startSubscriptions()
print('subscribed and running')

