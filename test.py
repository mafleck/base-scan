import pyjapc
import time
import pickle
import csv    

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from functools import partial
from scipy import constants as ctc
from scipy import optimize
import os
import functions as fs
import itertools
from sys import argv
import subprocess

########################################
# Set dry run/False real run/True
setRun = True
########################################

cycleUSER = 'LNA.USER.PBPRD1'
#japc = pyjapc.PyJapc(selector='', incaAcceleratorName='ELENA', noSet=(not setRun))
japc = pyjapc.PyJapc(selector='', incaAcceleratorName='', noSet=(not setRun))

try:
    japc.rbacLogin()
except:
    print('**** login failed, no set possible ****')
    pass

set_address = ['LNE07BEAM/BASE-H-OFFSET-mm',
               'LNE07BEAM/BASE-H-ANGLE-mrad',
               'LNE07BEAM/BASE-V-OFFSET-mm',
               'LNE07BEAM/BASE-V-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-H-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-V-ANGLE-mrad']

vals = []
for knob in set_address:
    vals.append(japc.getParam(knob))
print(vals)
