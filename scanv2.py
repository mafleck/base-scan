import pyjapc
import time
import pickle
import csv    

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from functools import partial
from scipy import constants as ctc
from scipy import optimize
import os
import functions as fs
import itertools
from sys import argv
import subprocess

########################################
# Set dry run/False real run/True
setRun = True
########################################

cycleUSER = 'LNA.USER.PBPRD1'
#cycleUSER = 'LNA.USER.MD4'
japc = pyjapc.PyJapc(selector='', incaAcceleratorName='ELENA', noSet=(not setRun))

try:
    japc.rbacLogin()
except:
    print('**** login failed, no set possible ****')
    pass

## read a file with grid points
scan_iterable = []
with open(argv[1]) as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        scan_iterable.append((float(row[0]),float(row[1]),float(row[2]),float(row[3])))

set_address = ['LNE07BEAM/BASE-H-OFFSET-mm',
               'LNE07BEAM/BASE-H-ANGLE-mrad',
               'LNE07BEAM/BASE-V-OFFSET-mm',
               'LNE07BEAM/BASE-V-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-H-ANGLE-mrad',
               'LNE07BEAM/BASE-SHIELD-V-ANGLE-mrad']

exp = fs.Setter(
    japc, 
    scan_iterable=scan_iterable, cycleUSER=cycleUSER,
    nrep=1, set_address=set_address)

japc.subscribeParam('LNE07.BSGW.0722/Acquisition#acqTime',
                    partial(fs.measurementDelay, exp, timingSelectorOverride=cycleUSER), 
                    timingSelectorOverride=cycleUSER)
japc.startSubscriptions()
print('subscribed and running')

